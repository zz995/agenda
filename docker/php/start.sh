#!/usr/bin/env bash

set -e

if [ $CONTAINER_ROLE = "queue" ]; then

    php bin/console messenger:consume

fi