<?php

namespace App\Event;

class ActionTracked
{
    private $id;
    private $userId;
    private $sourceLabel;
    private $dateCreated;
    private $temporaryId;

    public function __construct(string $id, string $sourceLabel, string $dateCreated, ?string $userId, ?string $temporaryId)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->temporaryId = $temporaryId;
        $this->sourceLabel = $sourceLabel;
        $this->dateCreated = $dateCreated;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTemporaryId(): ?string
    {
        return $this->temporaryId;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function getSourceLabel(): string
    {
        return $this->sourceLabel;
    }

    public function getDateCreated(): string
    {
        return $this->dateCreated;
    }
}