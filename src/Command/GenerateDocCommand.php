<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateDocCommand extends Command
{
    protected static $defaultName = 'app:generate-doc';

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $swagger = __DIR__ . '/../../vendor/bin/swagger';
        $source = __DIR__ . '/../../src';
        $target = __DIR__ . '/../../public/docs/swagger.json';
        passthru('"' . PHP_BINARY . '"' . " \"{$swagger}\" \"{$source}\" --output \"{$target}\"");
    }
}