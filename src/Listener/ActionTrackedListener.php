<?php

namespace App\Listener;

use App\Event\ActionTracked;
use App\UseCase\TrackingService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ActionTrackedListener implements MessageHandlerInterface
{
    private $service;

    public function __construct(TrackingService $service)
    {
        $this->service = $service;
    }

    public function __invoke(ActionTracked $event)
    {
        $this->service->register($event);
    }
}