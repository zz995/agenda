<?php

namespace App\Controller;

use App\Dto\UserLoginDto;
use App\Dto\UserRegisterDto;
use App\UseCase\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index(Security $security)
    {
        $user = $security->getUser();
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

    /**
     * @Route("/register", name="api_register", methods={"POST"})
     */
    public function register(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $json = $request->getContent();
        /** @var UserRegisterDto $dto */
        $dto = $serializer->deserialize($json, UserRegisterDto::class, 'json');

        $violations = $validator->validate($dto);
        if (count($violations) > 0) {
            return $this->json(['result' => false, 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        $this->service->register($dto);
        return $this->json(['result' => true, 'data' => []]);
    }
    /**
     * @SWG\Post(path="/register",
     *   tags={"user"},
     *   summary="User register",
     *   description="User register",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Register data",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/RegisterRequest")
     *   ),
     *   @SWG\Response(response="200", description="Success",
     *           @SWG\Property(
     *                 type="object",
     *                 @SWG\Property(property="data", type="array", @SWG\Items()),
     *                 @SWG\Property(property="result", type="boolean", description="value is true"),
     *           )
     *   ),
     * )
     */

    /**
     * @Route("/login", name="api_login", methods={"POST"})
     */
    public function login(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $json = $request->getContent();
        /** @var UserLoginDto $dto */
        $dto = $serializer->deserialize($json, UserLoginDto::class, 'json');
        $violations = $validator->validate($dto);
        if (count($violations) > 0) {
            $repl = $serializer->serialize($violations, 'json');
            return JsonResponse::fromJsonString($repl, Response::HTTP_BAD_REQUEST);
        }

        $token = $this->service->login($dto);
        if (empty($token)) {
            return $this->json(
                ['result' => false, 'message' => 'Nickname or password not correct'],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->json([
            'result' => true,
            'data' => [
                'token' => $token
            ]
        ]);
    }
    /**
     * @SWG\Post(path="/login",
     *   tags={"user"},
     *   summary="User login",
     *   description="User login",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Login data",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/LoginRequest")
     *   ),
     *   @SWG\Response(response="200", description="Success",
     *           @SWG\Property(
     *                 type="object",
     *                 @SWG\Property(property="data", type="object", ref="#/definitions/TokenResource"),
     *                 @SWG\Property(property="result", type="boolean", description="value is true"),
     *           )
     *   ),
     * )
     */
    /**
     * @SWG\Definition(
     *     definition="TokenResource",
     *     @SWG\Property(property="token", type="string")
     * )
     */
}
