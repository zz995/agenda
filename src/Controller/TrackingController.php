<?php

namespace App\Controller;

use App\Dto\TrackingDto;
use App\Dto\UserLoginDto;
use App\Event\ActionTracked;
use App\Security\User;
use App\UseCase\TrackingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TrackingController extends AbstractController
{
    private $service;

    public function __construct(TrackingService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/tracking", name="tracking", methods={"POST"})
     */
    public function index(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, Security $security)
    {
        $json = $request->getContent();
        /** @var TrackingDto $dto */
        $dto = $serializer->deserialize($json, TrackingDto::class, 'json');

        $violations = $validator->validate($dto);
        if (count($violations) > 0) {
            return $this->json(['result' => false, 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        /** @var User $user */
        $user = $security->getUser();
        if (empty($user) && empty($dto->temporary_id)) {
            return $this->json(['result' => false, 'message' => 'Need temporary id'], Response::HTTP_BAD_REQUEST);
        }

        $now = new \DateTime();

        $this->service->tracking($dto, $now, $user);

        return $this->json(['result' => true, 'data' => []]);
    }
    /**
     * @SWG\Post(path="/tracking",
     *   tags={"tracking"},
     *   summary="Tracking",
     *   description="Tracking",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *   @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Tracking data",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/TrackingRequest")
     *   ),
     *   @SWG\Response(response="200", description="Success",
     *           @SWG\Property(
     *                 type="object",
     *                 @SWG\Property(property="data", type="array", @SWG\Items()),
     *                 @SWG\Property(property="result", type="boolean", description="value is true"),
     *           )
     *   ),
     *   @SWG\Parameter(
     *      type="apiKey",
     *      in="header",
     *      name="Authorization"
     *   ),
     *   security={{"Bearer": {}}}
     * )
     */

    /**
     * @Route("/tracking/temporary-id", name="tracking_temporary_id", methods={"POST"})
     */
    public function temporaryId(Security $security)
    {
        $temporaryId = $this->service->temporaryId();
        return $this->json([
            'result' => true,
            'data' => [
                'temporaryId' => $temporaryId
            ]
        ]);
    }
    /**
     * @SWG\Post(path="/tracking/temporary-id",
     *   tags={"tracking"},
     *   summary="Receipt temporary id for not autorization user",
     *   description="Receipt temporary id for not autorization user",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *   @SWG\Response(response="200", description="Success",
     *           @SWG\Property(
     *                 type="object",
     *                 @SWG\Property(property="data", type="object", ref="#/definitions/TemporaryIdResource"),
     *                 @SWG\Property(property="result", type="boolean", description="value is true"),
     *           )
     *   ),
     * )
     */
    /**
     * @SWG\Definition(
     *     definition="TemporaryIdResource",
     *     @SWG\Property(property="temporaryId", type="string")
     * )
     */
}
