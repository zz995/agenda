<?php

namespace App\UseCase;

use App\Dto\TrackingDto;
use App\Dto\UserLoginDto;
use App\Dto\UserRegisterDto;
use App\Event\ActionTracked;
use App\Repository\TrackingRepository;
use App\Repository\UserRepository;
use App\Security\User;
use DateTime;
use Ramsey\Uuid\Uuid;
use SocialTech\StorageInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TrackingService
{
    private $repository;
    private $bus;

    public function __construct(TrackingRepository $repository, MessageBusInterface $bus)
    {
        $this->repository = $repository;
        $this->bus = $bus;
    }

    public function temporaryId(): string
    {
        return Uuid::uuid4()->toString();
    }

    public function tracking(TrackingDto $dto, DateTime $date, ?User $user)
    {
        $id = Uuid::uuid4()->toString();
        $userId = null;
        if (!empty($user)) {
            $userId = $user->getId();
        }

        $event = new ActionTracked(
            $id,
            $dto->source_label,
            $date->format('Y-m-d H:i:s'),
            $userId,
            $dto->temporary_id
        );
        $this->bus->dispatch($event);
    }

    public function register(ActionTracked $event)
    {
        $this->repository->add(
            $event->getId(),
            $event->getSourceLabel(),
            $event->getDateCreated(),
            $event->getUserId(),
            $event->getTemporaryId()
        );
    }
}