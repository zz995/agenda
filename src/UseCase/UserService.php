<?php

namespace App\UseCase;

use App\Dto\UserLoginDto;
use App\Dto\UserRegisterDto;
use App\Repository\UserRepository;
use App\Security\User;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private $repository;
    private $passwordEncoder;

    public function __construct(UserRepository $repository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->repository = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function register(UserRegisterDto $dto)
    {
        if ($this->repository->exists($dto->nickname)) {
            throw new \Exception('User already exists');
        }

        $user = new User();
        $user->setFirstname($dto->firstname);
        $user->setLastname($dto->lastname);
        $user->setNickname($dto->nickname);
        $user->setId(Uuid::uuid4()->toString());
        $user->setAge($dto->age);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $dto->password));
        $this->repository->add($user);
    }

    public function login(UserLoginDto $dto): ?string
    {
        $user = $this->repository->findByNickname($dto->nickname);

        if (empty($user)) {
            return null;
        }

        if (!$this->passwordEncoder->isPasswordValid($user, $dto->password)) {
            return null;
        };

        $user->setApiToken(bin2hex(random_bytes(60)));
        $this->repository->update($user);

        return $user->getApiToken();
    }
}