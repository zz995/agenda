<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UserRegisterDto
{
    /**
     * @Assert\NotBlank
     */
    public $firstname;
    /**
     * @Assert\NotBlank
     */
    public $lastname;
    /**
     * @Assert\NotBlank
     * @Assert\Regex("/^[a-zA-Z0-9\_]+$/")
     */
    public $nickname;
    /**
     * @Assert\NotBlank
     */
    public $age;
    /**
     * @Assert\NotBlank
     */
    public $password;
}
/**
 * @SWG\Definition(
 *     definition="RegisterRequest",
 *     title="RegisterRequest",
 *     type="object",
 *     @SWG\Property(property="firstname", type="string"),
 *     @SWG\Property(property="lastname", type="string"),
 *     @SWG\Property(property="nickname", type="string"),
 *     @SWG\Property(property="age", type="integer"),
 *     @SWG\Property(property="password", type="string"),
 * )
 */