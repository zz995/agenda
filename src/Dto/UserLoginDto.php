<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UserLoginDto
{
    /**
     * @Assert\NotBlank
     * @Assert\Regex("/^[a-zA-Z0-9\_]+$/")
     */
    public $nickname;
    /**
     * @Assert\NotBlank
     */
    public $password;
}
/**
 * @SWG\Definition(
 *     definition="LoginRequest",
 *     title="LoginRequest",
 *     type="object",
 *     @SWG\Property(property="nickname", type="string"),
 *     @SWG\Property(property="password", type="string"),
 * )
 */