<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class TrackingDto
{
    /**
     * @Assert\NotBlank
     */
    public $source_label;
    public $temporary_id;
}
/**
 * @SWG\Definition(
 *     definition="TrackingRequest",
 *     title="TrackingRequest",
 *     type="object",
 *     @SWG\Property(property="source_label", type="string"),
 *     @SWG\Property(property="temporary_id", type="string", description="required if user not authorized"),
 * )
 */