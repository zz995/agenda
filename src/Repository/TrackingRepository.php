<?php

namespace App\Repository;

use App\Security\User;
use SocialTech\StorageInterface;
use Symfony\Component\Serializer\SerializerInterface;

class TrackingRepository
{
    private $path = __DIR__ . '/../../storage/tracking';
    private $serializer;
    private $storage;

    public function __construct(SerializerInterface $serializer, StorageInterface $storage)
    {
        $this->serializer = $serializer;
        $this->storage = $storage;
    }

    public function add(string $id, string $sourceLabel, string $dateCreated, ?string $userId, ?string $temporaryId)
    {
        $data = [
            'id' => $id,
            'user_id' => $userId,
            'temporary_id' => $temporaryId,
            'source_label' => $sourceLabel,
            'date_created' => $dateCreated,
        ];

        $json = json_encode($data);
        $pathToFile = $this->pathToFile($id);
        $this->storage->store($pathToFile, $json);
    }

    private function pathToFile(string $fileName)
    {
        return $this->path . '/' . $fileName . '.json';
    }
}