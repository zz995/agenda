<?php

namespace App\Repository;

use App\Security\User;
use Symfony\Component\Serializer\SerializerInterface;

class UserRepository
{
    private $path = __DIR__ . '/../../storage/users';
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function findByApiToken(string $apiToken): ?User
    {
        $nicknames = $this->allNicknames();
        foreach ($nicknames as $nickname) {
            $user = $this->findByNickname($nickname);
            if (empty($user)) {
                continue;
            }

            if ($user->isEqualApiToken($apiToken)) {
                return $user;
            }
        }

        return null;
    }

    public function findByNickname(string $nickname): ?User
    {
        $json = $this->loadData($nickname);
        if (is_null($json)) {
            return null;
        }

        /** @var User $user */
        $user = $this->serializer->deserialize($json, User::class, 'json');
        return $user;
    }

    public function add(User $user)
    {
        $json = $this->serializer->serialize($user, 'json');
        $this->storeData($json, $user->getUsername());
    }

    public function update(User $user)
    {
        $json = $this->serializer->serialize($user, 'json');
        $this->storeData($json, $user->getUsername());
    }

    public function exists(string $nickname): bool
    {
        $pathToFile = $this->pathToFile($nickname);
        return file_exists($pathToFile);
    }

    private function allNicknames(): array
    {
        $nicknames = [];
        $files = scandir($this->path);
        foreach ($files as $file) {
            [$nickname, $extension] = @explode('.', $file);
            if ($extension != 'json') {
                continue;
            }

            $nicknames[] = $nickname;
        }

        return $nicknames;
    }

    private function storeData(string $serializedData, string $fileName)
    {
        $pathToFile = $this->pathToFile($fileName);
        file_put_contents($pathToFile, $serializedData, LOCK_EX);
    }

    private function loadData(string $fileName): ?string
    {
        if (!$this->exists($fileName)) {
            return null;
        }

        $pathToFile = $this->pathToFile($fileName);
        return file_get_contents($pathToFile);
    }

    private function pathToFile(string $fileName)
    {
        return $this->path . '/' . $fileName . '.json';
    }
}